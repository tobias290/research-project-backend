from api.router import Router
from ast import literal_eval
import requests
import random
import sqlite3

@Router.get("/")
def index(request):
    with open("public/index.html") as file:
        return file.read()


@Router.get("/get-related/{objectId}", api=True)
def get_related(request):
    base_url = "https://www.vam.ac.uk/api/json/museumobject"

    # Get main object details
    details = requests.get(f"{base_url}/{request.params['objectId']}").json()[0]["fields"]

    # Get Related content
    artist_results = requests.get(f"{base_url}/search?namesearch={details['artist']}&images=1").json()["records"] if details["artist"] != "" else None
    object_results = requests.get(f"{base_url}/search?objectnamesearch={details['object']}&images=1").json()["records"] if details["object"] != "" else None
    place_results = requests.get(f"{base_url}/search?placesearch={details['place']}&images=1").json()["records"] if details["place"] != "" else None
    material_results = requests.get(f"{base_url}/search?materialsearch={details['materials_techniques']}&images=1").json()["records"] if details["materials_techniques"] != "" else None

    SAMPLE_SIZE = 20

    # Get a sample of five items from each related subject's results
    sample = {
        "artist": random.sample(artist_results, SAMPLE_SIZE if len(artist_results) >= SAMPLE_SIZE else len(artist_results)) if artist_results is not None else None,
        "object": random.sample(object_results, SAMPLE_SIZE if len(object_results) >= SAMPLE_SIZE else len(object_results)) if object_results is not None else None,
        "place": random.sample(place_results, SAMPLE_SIZE if len(place_results) >= SAMPLE_SIZE else len(place_results)) if place_results is not None else None,
        "material": random.sample(material_results, SAMPLE_SIZE if len(material_results) >= SAMPLE_SIZE else len(material_results)) if material_results is not None else None,
    }

    return sample


@Router.get("/get-word-count/{item}", api=True)
def get_word_count(request):
    if request.params["item"] not in ["place", "artist", "object", "country"]:
        return {"error": "Invalid request!"}

    db = sqlite3.connect("data.db")
    data = db.cursor().execute(f"SELECT * FROM {request.params['item']}_word_count").fetchall()
    return dict(data)


@Router.get("/map-data", api=True)
def map_data(request):
    with open("TM_WORLD_BORDERS-0.json") as json:
        return dict(literal_eval(json.read()))