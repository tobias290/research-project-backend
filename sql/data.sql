CREATE TABLE data
(
    object_number       TEXT NOT NULL PRIMARY KEY ,
    title               TEXT NOT NULL,
    slug                TEXT NOT NULL,
    artist              TEXT NOT NULL,
    place               TEXT NOT NULL,
    date_text           TEXT NOT NULL,
    location            TEXT NOT NULL,
    object              TEXT NOT NULL,
    museum_number       TEXT NOT NULL,
    museum_number_token TEXT NOT NULL,
    collection_code     TEXT NOT NULL,
    primary_image_id    TEXT NOT NULL
);