CREATE TABLE place_word_count
(
    place TEXT NOT NULL PRIMARY KEY,
    count  INT  NOT NULL
);