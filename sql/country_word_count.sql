CREATE TABLE country_word_count
(
    country TEXT NOT NULL PRIMARY KEY,
    count  INT  NOT NULL
);