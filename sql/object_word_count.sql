CREATE TABLE object_word_count
(
    object TEXT NOT NULL PRIMARY KEY,
    count  INT  NOT NULL
);