CREATE TABLE artist_word_count
(
    artist TEXT NOT NULL PRIMARY KEY,
    count  INT  NOT NULL
);