import http.server as https
import socketserver
import json
from api.router import Router
from api.request import Request
from urllib.parse import parse_qs
import os


class Server(https.BaseHTTPRequestHandler):
    """
    Basic HTTP Server
    """
    def __init__(self, *args, **kwargs):
        self.__router = Router()
        self.__content_types = {
            "js": "application/javascript",
            "json": "application/json",
            "html": "text/html",
            "css": "text/css",
        }
        super().__init__(*args, **kwargs)

    @classmethod
    def run(cls, host, port):
        with socketserver.TCPServer((host, port), cls) as httpd:
            print("Serving at port:", port)
            httpd.serve_forever()

    def set_headers(self, http_code: int, headers: dict):
        """
        Sets the headers
        :param http_code: HTTP response code
        :param headers: Headers dictionary
        """
        self.send_response(http_code)

        for key, header in headers.items():
            self.send_header(key, header)

        self.end_headers()

    def method(self, method: str):
        """
        Generic method function that runs on any request
        :param method: HTTP method
        """
        for route in self.__router:
            path = route["path"]

            is_match, variables = path.is_match(self.path)

            if is_match and method == route["method"]:
                query_string = {k: v[0] for k, v in parse_qs(self.path.split("?")[1]).items()} if "?" in self.path else {}

                if route["api"]:
                    self.set_headers(200, {"Content-type": self.__content_types["json"], "Access-Control-Allow-Origin": "*"})
                    self.wfile.write(str.encode(json.dumps(route["func"](Request(variables, query_string)))))
                else:
                    self.set_headers(200, {"Content-type": self.__content_types["html"], "Access-Control-Allow-Origin": "*"})
                    self.wfile.write(str.encode(route["func"](Request(variables, query_string))))
        else:
            # If route doesn't exist it might be a public file
            possible_public_file = f"public{self.path}"

            # Check file exists in public folder
            if os.path.exists(possible_public_file):
                try:
                    # Get the contents and return it as a http response
                    with open(possible_public_file, encoding="utf8") as public_file:
                        file_type = possible_public_file.split(".")[-1]

                        self.set_headers(200, {
                            "Content-type": self.__content_types[file_type] if file_type in self.__content_types.keys() else "text/plain",
                            "Access-Control-Allow-Origin": "*"}
                         )
                        self.wfile.write(str.encode(public_file.read()))
                except PermissionError:
                    print(f"Cannot open {possible_public_file}")

    do_GET = lambda self: self.method("get")
    do_POST = lambda self: self.method("post")
    do_PUT = lambda self: self.method("put")
    do_DELETE = lambda self: self.method("delete")