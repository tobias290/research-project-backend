class Path:
    """
    Defines a single path route
    """

    def __init__(self, path: str):
        """
        :param path: Path url
        """
        self.__path: str = path

    def __str__(self):
        return self.__path

    def __is_var(self, part: str):
        """
        Returns true is the current path part is a variable
        :param part: Part of the path to check
        :return: Returns true is the current part is a variable and false is not
        """
        return len(part) != 0 and part[0] == "{" and part[-1] == "}"

    def __extract_var(self, path_part, current_path_part):
        """
        Extracts the variable from the the current path part
        :param path_part: Part of the path holding the variable name
        :param current_path_part: Part of the current path that holds the actual variable value
        :return: Returns a dict with the variable
        """
        return {path_part[1:-1]: current_path_part}

    def is_match(self, current_path: str):
        """
        Checks if the current path matches a particular path
        :param current_path: Current path URL
        :return: Returns true if it is a match and false is not (also returns the variables list)
        """
        route_variables = {}
        split_path = self.__path.split("/")
        split_current_path = current_path.split("?")[0].split("/") if "?" in current_path else current_path.split("/")

        if len(split_path) != len(split_current_path):
            return False, route_variables

        is_match = False

        # Splits the path and loops over both the current and examined path
        for path_part, current_path_part in zip(split_path[1:], split_current_path[1:]):
            # Checks if the current part is a variable
            # Anything matches here just need to extract the variable
            if self.__is_var(path_part):
                route_variables.update(self.__extract_var(path_part, current_path_part))
                is_match = True
                continue

            if path_part == current_path_part:
                is_match = True
            else:
                is_match = False
                break

        return is_match, route_variables


