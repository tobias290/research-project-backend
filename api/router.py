import functools
from api.path import Path


class Router:
    __routes = []

    def __iter__(self):
        return iter(Router.__routes)

    @staticmethod
    def __route(path: str, method: str, api: bool):
        """
        Generic route defining method
        :param path: Path url
        :param method: HTTP method
        :param api: If true then this route is an API route and will render JSON content, else it will render html
        :return:
        """
        def wrapper(func):
            Router.__routes.append({"func": func, "path": Path(path), "method": method, "api": api})

            @functools.wraps(func)
            def inner(*args, **kwargs):
                return func(*args, **kwargs)
            return inner
        return wrapper

    @staticmethod
    def get(path: str, api: bool = False):
        """
        Defines a route that matched the "GET" method
        :param path: Path of the route
        :param api: If true then this route is an API route and will render JSON content, else it will render html
        """
        return Router.__route(path, "get", api)

    @staticmethod
    def post(path: str, api: bool = False):
        """
        Defines a route that matched the "POST" method
        :param path: Path of the route
        :param api: If true then this route is an API route and will render JSON content, else it will render html
        """
        return Router.__route(path, "post", api)

    @staticmethod
    def put(path: str, api: bool = False):
        """
        Defines a route that matched the "PUT" method
        :param path: Path of the route
        :param api: If true then this route is an API route and will render JSON content, else it will render html
        """
        return Router.__route(path, "put", api)

    @staticmethod
    def delete(path: str, api: bool = False):
        """
        Defines a route that matched the "DELETE" method
        :param path: Path of the route
        :param api: If true then this route is an API route and will render JSON content, else it will render html
        """
        return Router.__route(path, "delete", api)

    @staticmethod
    def get_routes():
        """
        :return: Returns all the routes
        """
        return Router.__routes
