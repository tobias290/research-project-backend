class Request:
    """
    Basic request class
    """
    def __init__(self, params: dict, query_string: dict):
        """
        :param params: Parameters in the URL
        :param query_string: Query string
        """
        self.__params = params
        self.__query_string = query_string

    @property
    def params(self) -> dict:
        return self.__params

    @property
    def query_string(self):
        return self.__query_string
