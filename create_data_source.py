from data_processing import APIDataMiner, processors
from os import listdir
from os.path import isfile, join
import sqlite3
import time

DB_NAME = "data.db"


def create_db():
    f = open(DB_NAME, "w")
    f.close()


def create_tables():
    for sql_file in listdir("sql"):
        sql_file = join("sql", sql_file)

        if isfile(sql_file):
            with open(sql_file, "r") as sql_file_contents:
                sql = sql_file_contents.read()

                db = sqlite3.connect(DB_NAME)
                cursor = db.cursor()
                cursor.execute(sql)


def add_data():
    for processor in processors:
        if processor == APIDataMiner:
            processor(db="data.db", store_limit=14985).run()
        else:
            processor(db="data.db").run()


if __name__ == "__main__":
    create_db()
    create_tables()
    add_data()
