## Requirements

* Python >= 3.8
    * Packages:
        * sqlite3
* JavaScript Enabled


## Pre Run instructions

If for any reason the **data.db** file is not present in the root of this project. Run the **create_data_source.py** file.
This should create a **data.db** sqlite file which holds all the data the backend needs.  


## Run Instructions

To run just execute the **main.py** script and load your browser to **localhost:8000** and the main page will load. 


## Build Instructions

If you want to rebuild the front end project follow its readme and run **yarn build** or **npm build** then copy the contents of the build directory into the public directory here on the backend.

