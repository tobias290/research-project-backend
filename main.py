import api
import routes


if __name__ == "__main__":
    HOST = ""
    PORT = 8000

    api.Server.run(HOST, PORT)
