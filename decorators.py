def time_function(func):
    """
    Times how a long the function takes to run
    """
    def inner(*args, **kwargs):
        import time

        start = time.time()
        return_data = func(*args, **kwargs)
        end = time.time()

        print("Time:", round(end - start, 2), "seconds\n")
        return return_data

    return inner
