from .api_data_miner import APIDataMiner
from .artist_word_counter import ArtistWordCounter
from .country_word_counter import CountryWordCounter
from .object_word_counter import ObjectWordCounter
from .place_word_counter import PlaceWordCounter

processors = [
    APIDataMiner,
    ArtistWordCounter,
    CountryWordCounter,
    ObjectWordCounter,
    PlaceWordCounter,
]
