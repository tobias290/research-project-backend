import sqlite3

import requests

import decorators
from .miner import Miner


class APIDataMiner(Miner):
    """
    Processes the data and add its to a SQLite database
    """
    def __init__(self, db: str = "../data.db", offset: int = 0, limit: int = 45, store_limit: int = 1_000_000):
        """
        :param db: Path to the db file
        :param offset: Starting offset. (Offset is used to know where to start using pagination)
        :param limit: How many records to get per request (Max is 45)
        :param store_limit: Maximum records to get
        """
        super().__init__(db)
        self.__offset: int = offset
        self.__limit: int = limit
        self.__store_limit: int = store_limit

    @decorators.time_function
    def run(self):
        """
        Mines the data
        """
        db = sqlite3.connect(self._db)
        cursor = db.cursor()

        while self.__offset < self.__store_limit:
            url = f"https://www.vam.ac.uk/api/json/museumobject/search?after=-3496&limit={self.__limit}&offset={self.__offset}&images=1"
            request = requests.get(url).json()
            print(f"% Complete: {round((self.__offset / self.__store_limit) * 100, 2)}%, Offset: {self.__offset}, URL: {url}")

            for record in request["records"]:
                fields = record["fields"]
                cursor.execute("INSERT INTO data VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", (
                    fields["object_number"],
                    fields["title"],
                    fields["slug"],
                    fields["artist"],
                    fields["place"],
                    fields["date_text"],
                    fields["location"],
                    fields["object"],
                    fields["museum_number"],
                    fields["museum_number_token"],
                    fields["collection_code"],
                    fields["primary_image_id"],
                ))

            self.__offset += self.__limit
            db.commit()


if __name__ == "__main__":
    APIDataMiner(store_limit=14985).run()
