import sqlite3
from collections import defaultdict

import mysql.connector

import decorators
from .miner import Miner


class CountryWordCounter(Miner):
    @decorators.time_function
    def run(self):
        db = sqlite3.connect(self._db)
        cursor = db.cursor()

        mysql_db = mysql.connector.connect(
            host="localhost",
            user="root",
            passwd="admin",
            database="ip2location"
        )
        mysql_cursor = mysql_db.cursor(buffered=True)

        cursor.execute("SELECT place FROM place_word_count")
        rows = cursor.fetchall()

        data = defaultdict(int)

        for i, row in enumerate(rows):
            place = row[0]
            print(f"{place} - {i / 500 * 100}% Complete")

            mysql_cursor.execute(f"SELECT country_name FROM ip2location_db5 WHERE country_name = \"{place}\"")
            result = cursor.fetchall()

            if len(result) != 0:
                data[place] += 1
            else:
                mysql_cursor.execute(f"SELECT country_name FROM ip2location_db5 WHERE city_name = \"{place}\" LIMIT 1")
                result = mysql_cursor.fetchall()

                if len(result) != 0:
                    data[result[0][0].decode("utf-8")] += 1

        for place, count in data.items():
            cursor.execute("INSERT INTO country_word_count VALUES (?, ?)", (place, count))

        db.commit()


if __name__ == "__main__":
    CountryWordCounter().run()
