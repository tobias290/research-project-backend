import sqlite3
from collections import defaultdict

import decorators
from .miner import Miner


class ArtistWordCounter(Miner):
    @decorators.time_function
    def run(self):
        db = sqlite3.connect(self._db)
        cursor = db.cursor()

        cursor.execute("SELECT artist FROM data")
        rows = cursor.fetchall()

        data = defaultdict(int)

        for row in rows:
            data[row[0]] += 1

        for artist, count in data.items():
            cursor.execute("INSERT INTO artist_word_count VALUES (?, ?)", (artist, count))

        db.commit()


if __name__ == "__main__":
    ArtistWordCounter().run()
