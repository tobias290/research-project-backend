import sqlite3
from collections import defaultdict

import decorators
from .miner import Miner


class PlaceWordCounter(Miner):
    @decorators.time_function
    def run(self):
        db = sqlite3.connect(self._db)
        cursor = db.cursor()

        cursor.execute("SELECT place FROM data")
        rows = cursor.fetchall()

        data = defaultdict(int)

        for row in rows:
            data[row[0]] += 1

        for place, count in data.items():
            cursor.execute("INSERT INTO place_word_count VALUES (?, ?)", (place, count))

        db.commit()


if __name__ == "__main__":
    PlaceWordCounter().run()
